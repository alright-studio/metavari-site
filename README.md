# Metavari Landing Site 

## Requirements
- Node 8+
- Yarn 

## Local Development

1. Install Yarn dependencies
```
$ yarn install
```
2. Start local development server for live reload changes. Navigate to [http://localhost:8080](http://localhost:8080).
```
$ yarn start
```

## Deploying to Production

Changes to the `master` branch are automatically deployed to production via Netlify. 

## Editing Website Data

There are two places where website data is stored:

All text data is stored in the site databases, found in `src/databases/<DATABASENAME>.json`.

All photographs should be stored in `public/content`. These should be compressed as much as possible, as they'll be served directly to the browser.
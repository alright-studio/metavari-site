import '../styles/main.scss';

import React, {render} from 'react';
import {
	HashRouter as Router,
	Route,
	Switch,
} from 'react-router-dom';
import AnchorLinks from '../components/AnchorLinks';
import Releases from '../components/Releases';
import Videos from '../components/Videos';
import ReleaseModal from '../components/ReleaseModal';
import VideoModal from '../components/VideoModal';
import releaseDatabase from '../databases/releases.json';
import videoDatabase from '../databases/videos.json';
import withProps from 'recompose/withProps';

const ConnectedReleaseModal = withProps({
	tags: releaseDatabase.tags,
	releases: releaseDatabase.releases,
})(ReleaseModal);

const ConnectedVideoModal = withProps({
	tags: videoDatabase.tags,
	videos: videoDatabase.videos,
})(VideoModal);

//
// === Bind UI ===
//

// Anchor Links
const anchorLinks = document.querySelectorAll('a[href^="#"]');
// Releases	
const mountReleasesEl = document.getElementById('mount-releases');
// Videos
const mountVideosEl = document.getElementById('mount-videos');
// Modals
const mountModalEl = document.getElementById('mount-modal-router');

//
// === Initialize UI ===
//

// Timeout to let anchor links work
setTimeout(() => {
	AnchorLinks({
		anchorEls: anchorLinks,
	});
	
	render((
		<Router>
			<Releases
				tags={releaseDatabase.tags}
				typeTags={releaseDatabase.typeTags}
				releases={releaseDatabase.releases}
			/>
		</Router>
	), mountReleasesEl);
	
	render((
		<Router>
			<Videos
				tags={videoDatabase.tags}
				typeTags={videoDatabase.typeTags}
				videos={videoDatabase.videos}
			/>
		</Router>
	), mountVideosEl);
	
	render((
		<Router>
			<div>
				<Switch>
					<Route
						exact
						path="/releases/:releaseId"
						render={props => (
							<ConnectedReleaseModal
								key={props.match.params.releaseId}
								{...props}
							/>
						)}
					/>
					<Route
						exact
						path="/videos/:videoId"
						render={props => (
							<ConnectedVideoModal
								key={props.match.params.videoId}
								{...props}
							/>
						)}
					/>
				</Switch>
			</div>
		</Router>
	), mountModalEl);
}, 100);

//
// === Site Credit ===
//

console.log(
	'Site Credit' + '\n' +
	'===========' + '\n' +
	'Design, Development : https://alright.studio'
);
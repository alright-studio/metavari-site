export default {
	xs: 400,
	sm: 600,
	md: 800,
	lg: 1000,
	xl: 1200,
};
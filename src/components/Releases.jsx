import React from 'react';
import {Link} from 'react-router-dom';
import withState from 'recompose/withState';
import compose from 'recompose/compose';
import reduce from 'lodash/reduce';
import filter from 'lodash/filter';
import without from 'lodash/without';
import find from 'lodash/find';
import includes from 'lodash/includes';
import classNames from 'classnames';

const TagFilters = ({
	tags,
	typeTags,
	activeTag,
	setActiveTag,
}) => {
	const typeTagFilters = reduce(typeTags, (typeTagFilters, tag, index) => {
		const tagButton = (
			<button
				key={tag.id}
				onClick={() => setActiveTag(tag.id)}
				name={tag.title}
				type="button"
				className={classNames('link--secondary ws--nowrap', {
					'o50': tag.id !== activeTag
				})}>
				{tag.title}
			</button>
		);

		const divider = (
			<span
				key={`divider-${index}`}
				className="o50">
				{' / '}
			</span>
		);

		if (
			typeTags.length > 1 &&
			index < typeTags.length - 1
		) {
			return [
				...typeTagFilters,
				tagButton,
				divider,
			];
		} else {
			return [
				...typeTagFilters,
				tagButton,
			];
		}
	}, []);

	const releaseTagButtons = tags.map(tag => (
		<button
			key={tag.id}
			onClick={() => setActiveTag(tag.id)}
			name={tag.title}
			type="button"
			className={classNames('db link--secondary ws--nowrap', {
				'o50': tag.id !== activeTag
			})}>
			<span className="table__glyph dib vam mr1">
				{tag.glyph}
			</span>
			<span className="vam">
				{tag.title}
			</span>
		</button>
	));

	return (
		<div>
			<h2 className="color--gray">Filter by Type:</h2>
			<div className="mb2">
				{typeTagFilters}
			</div>
			
			<h2 className="color--gray">Filter by Tag:</h2>
			{releaseTagButtons}
		</div>
	);
};

const ReleaseRow = ({
	id,
	title,
	subtitle,
	tags,
	format,
	label,
	year,
	image,
	allTags,
	externalLink,
	enablePopup,
}) => {
	const releaseTagsWithoutFeatured = without(tags, 'featured');

	const tag = find(allTags, {id: releaseTagsWithoutFeatured[0]});
	const tagGlyph = (
		<span className="db">
			{tag.glyph}
		</span>
	);

	const releaseImage = image && (
		<img
			alt={title}
			className="table__hover pe--none"
			src={image}
		/>
	);

	const releaseContents = (
		<div className="row pt2 pb2 push-up">
			<div className="col c6 hide--sm color--gray">
				Code:
			</div>
			<div className="col c6 c2--sm c1--md">
				<p>{id}</p>
			</div>

			<div className="divider hide--sm" />

			<div className="col c6 hide--sm color--gray">
				Title:
			</div>
			<div className="col c6 c4--sm c5--md helvetica--body">
				<p>{title}</p>
				<span>{subtitle}</span>
			</div>

			<div className="divider hide--sm" />

			<div className="col c6 hide--sm color--gray">
				Tag:
			</div>
			<div className="col c6 c1--sm">
				{tagGlyph}
				<p className="show--sm"><br/></p>
			</div>

			<div className="divider hide--sm" />

			<div className="col c6 hide--sm color--gray">
				Format:
			</div>
			<div className="col c6 c2--sm">
				<p>{format.join(', ')}</p>
			</div>

			<div className="divider hide--sm" />

			<div className="col c6 hide--sm color--gray">
				Label / Partner:
			</div>
			<div className="col c6 c2--sm">
				<p>{label}</p>
			</div>

			<div className="divider hide--sm" />

			<div className="col c6 hide--sm color--gray">
				Year:
			</div>
			<div className="col c6 c1--sm tr--sm">
				<p>{year}</p>
			</div>
		</div>
	);

	if (externalLink) {
		return (
			<a
				target="_blank"
				href={externalLink}
				className="table__row db">
				{releaseImage}
				{releaseContents}
				<div className="border--bottom" />
			</a>
		);
	} else if (enablePopup) {
		return (
			<Link
				to={`/releases/${id}`}
				className="table__row db">
				{releaseImage}
				{releaseContents}
				<div className="border--bottom" />
			</Link>
		);
	}

	return (
		<div className="table__row">
			{releaseImage}
			{releaseContents}
			<div className="border--bottom" />
		</div>
	);
};

const Releases = ({
	tags,
	typeTags,
	releases,
	activeTag,
	setActiveTag,
}) => {
	let filteredReleases = releases;

	if (activeTag) {
		filteredReleases = filter(releases, release => includes(release.tags, activeTag));
	}

	const releaseRows = filteredReleases.map(release => (
		<ReleaseRow
			key={release.id}
			allTags={tags}
			{...release}
		/>
	));

	const releaseHeader = (
		<div>
			<div className="row color--gray pb1 show--sm">
				<div className="col c2 c1--md">
					<p>Code</p>
				</div>
				<div className="col c4 c5--md">
					<p>Title</p>
				</div>
				<div className="col c1">
					<p>Tag</p>
				</div>
				<div className="col c2">
					<p>Format</p>
				</div>
				<div className="col c2">
					<p>Label / Partner</p>
				</div>
				<div className="col c1 tr">
					<p>Year</p>
				</div>
			</div>
			<div className="border--bottom" />
		</div>
	);

	return (
		<div className="row">
			<div className="col c12 c2--md">
				<TagFilters
					tags={tags}
					typeTags={typeTags}
					activeTag={activeTag}
					setActiveTag={setActiveTag}
				/>
			</div>
			<div className="col c12 c10--md mt4 mt0--md">
				{releaseHeader}
				{releaseRows}
			</div>
		</div>
	);
};

export default compose(
	withState('activeTag', 'setActiveTag', 'featured')
)(Releases);
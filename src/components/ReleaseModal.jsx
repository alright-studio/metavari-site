import React from 'react';
import {Link} from 'react-router-dom';
import classNames from 'classnames';
import lifecycle from 'recompose/lifecycle';
import find from 'lodash/find';
import compact from 'lodash/compact';

const ReleaseModal = ({
	releases,
	tags,
	match,
}) => {
	const release = find(releases, {id: match.params.releaseId});

	if (!release) return null;

	const modalUnderlay = (
		<Link
			to="/"
			className="db modal__underlay"
		/>
	);

	const releaseHeader = (
		<div className="row mb1">
			<div className="col c8">
				<h1 className="helvetica--header ws--pre-line uppercase">
					{release.title}
				</h1>
			</div>
			<div className="col c4">
				<p className="tr">
					<Link to="/">
						[Close]
					</Link>
				</p>
			</div>
		</div>
	);

	let releaseTags = release.tags.map(tag => find(tags, {id: tag}));
	releaseTags = compact(releaseTags);
	
	const releaseTagTitles = releaseTags.map(tag => tag ? tag.title : null);

	const releaseDetails = (
		<p>
			{release.id}
			<span className="color--gray"> / </span>
			<span className="color--gray">{release.date} / {releaseTagTitles.join(', ')} / {release.label}</span>
		</p>
	);

	let currentTrackNumber = 0;

	const trackListSections = release.tracklists.map((tracklist, index) => {
		const trackItems = tracklist.tracks.map((track, index) => {
			currentTrackNumber += 1;

			return (
				<div
					key={index}
					className="media media--horizontal gutter--none">
					<div className="media__fixed pr1">
						{currentTrackNumber < 10 ? '0' : null}{currentTrackNumber}.
					</div>
					<div className="media__fluid">
						{track}
					</div>
				</div>
			);
		});

		return (
			<div
				key={index}
				className="mb2">
				<h3 className="color--black helvetica--body">{tracklist.title}</h3>
				{trackItems}
			</div>
		);
	});

	const linkListSections = release.linkLists.map((linkList, index) => {
		const links = linkList.links.map((link, index) => {
			const linkContents = link.external ? (
				<a
					href={link.url}
					target="_blank"
					className="link--primary">
					{link.title}
				</a>
			) : (
				<a
					href={link.url}
					className="link--primary">
					{link.title}
				</a>
			);

			return (
				<li
					key={index}
					className="list__item">
					↳ {linkContents}
				</li>
			);
		});

		return (
			<div className="mt2">
				<h3 className="color--black helvetica--body">{linkList.title}</h3>
				<ul className="list list--horizontal list--vertical-gutter gutter--tiny">
					{links}
				</ul>
			</div>
		);
	});

	const imageCol = release.image && (
		<div className="col c6--md fl">
			<img
				className="full-width"
				src={release.image}
			/>
		</div>
	);

	const modal = (
		<div className="modal">
			<div className="p2 p4--md">
				{releaseHeader}
				{releaseDetails}
				<p><br/></p>
				<p className="pr10--lg ws--pre-line">{release.description}</p>
			</div>
			<div className="row gutter--none direction--rev--md align--bottom">
				<div className={classNames('col', {
					'c6--md': release.image
				})}>
					<div className="pl2 pr2 pl4--md pr4--md pb2 pb4--md">
						{trackListSections}
						{linkListSections}
					</div>
				</div>
				{imageCol}
			</div>
		</div>
	);

	return (
		<div>
			{modalUnderlay}
			{modal}
		</div>
	);
};

export default lifecycle({
	componentDidMount() {
		window.document.documentElement.classList.add('of--hidden');
		window.document.body.classList.add('of--hidden');
	},

	componentWillUnmount() {
		window.document.documentElement.classList.remove('of--hidden');
		window.document.body.classList.remove('of--hidden');
	}
})(ReleaseModal);
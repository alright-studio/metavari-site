import forEach from 'lodash/forEach';
import scrollTo from '../utils/scrollTo';
import offset from '../utils/offset';

const SCROLL_DURATION = 1000;

const onClickAnchor = event => {
	// Find element w/ triggerSelector
	let el = event.path ? event.path[0] : event.target;
	while (el && el.matches && !el.matches('a[href]')) el = el.parentNode;
	if (!el || !el.matches || !el.matches('a[href]')) return;

	// Prevent scrolling under certain conditions
	if (event.which > 1 || event.metaKey || event.ctrlKey || event.shiftKey || event.altKey) return;
	if (el.target && el.target === '_blank') return;
	if (el.getAttribute && typeof el.getAttribute('download') === 'string') return;
	if (el.href.indexOf('#') < 0) return;
	
	const scrollTarget = document.querySelector(el.hash);

	if (!scrollTarget) return;

	event.preventDefault();

	// Scroll to target
	scrollTo(offset(scrollTarget).top, SCROLL_DURATION);
};


export default ({
	anchorEls
}) => {
	forEach(anchorEls, anchor => {
		anchor.addEventListener('click', onClickAnchor);
	});
};
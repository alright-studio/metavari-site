import React from 'react';
import {Link} from 'react-router-dom';
import lifecycle from 'recompose/lifecycle';
import find from 'lodash/find';

const ReleaseModal = ({
	videos,
	tags,
	match,
}) => {
	const video = find(videos, {id: match.params.videoId});

	if (!video) return null;

	const modalUnderlay = (
		<Link
			to="/"
			className="db modal__underlay"
		/>
	);

	const releaseHeader = (
		<div className="row">
			<div className="col c8">
				<h1 className="helvetica--header ws--pre-line uppercase">
					{video.title}
				</h1>
				
			</div>
			<div className="col c4">
				<p className="tr">
					<Link to="/">
						[Close]
					</Link>
				</p>
			</div>
			<div className="col mt1">
				{video.id} / <span className="color--gray">{video.year} / {video.label}</span>
				<p className="mt2 ws--pre-line">
				{video.description}
				</p>
				</div>
		</div>
	);

	const videoIframe = (
		<div className="video">
			<iframe
				width="560"
				height="315"
				src={`https://www.youtube.com/embed/${video.youtubeVideoId}?rel=0;&autoplay=1`}
				frameBorder="0"
				allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
				allowFullScreen
			/>
		</div>
	);

	const modal = (
		<div className="modal">
			<div className="p2 p4--md">
				{releaseHeader}
			</div>
			{videoIframe}
		</div>
	);

	return (
		<div>
			{modalUnderlay}
			{modal}
		</div>
	);
};

export default lifecycle({
	componentDidMount() {
		window.document.documentElement.classList.add('of--hidden');
		window.document.body.classList.add('of--hidden');
	},

	componentWillUnmount() {
		window.document.documentElement.classList.remove('of--hidden');
		window.document.body.classList.remove('of--hidden');
	}
})(ReleaseModal);
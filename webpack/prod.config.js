const path = require('path');
const reduce = require('lodash/reduce');
const fs = require('fs');
const autoprefixer = require('autoprefixer');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

//
// === Constants ===
//

const ROOT_DIR = path.resolve(__dirname, '..');
const ENTRYPOINT_DIR = path.resolve(ROOT_DIR, 'src', 'entrypoints');
const DIST_DIR = path.resolve(ROOT_DIR, 'dist');

//
// === Get entrypoints from folder ===
//

const entrypointFileNames = fs.readdirSync(ENTRYPOINT_DIR).filter(file => {
	return file.match(/.*\.js$/);
});

const entrypoints = reduce(entrypointFileNames, (entrypoints, fileName) => {
	const entrypointName = fileName.replace('.js', '');

	return {
		...entrypoints,
		[entrypointName]: path.resolve(ENTRYPOINT_DIR, fileName),
	};
}, {});

//
// === Sass Loader Options ===
//

const postCSSOptions = {
	plugins: () => [
		autoprefixer({
			browsers: [
				'>1%',
				'last 4 versions',
				'not ie < 9'
			]
		})
	]
};

const sassLoaderOptions = {
	includePaths: [
		path.resolve(ROOT_DIR, 'node_modules'),
	]
};

const sassLoader = {
	test: /\.(scss|css)$/,
	use: [
		MiniCssExtractPlugin.loader,
		'css-loader',
		{
			loader: 'postcss-loader',
			options: postCSSOptions
		},
		{
			loader: 'sass-loader',
			options: sassLoaderOptions
		}
	],
	include: path.resolve(ROOT_DIR, 'src')
};

//
// === JS Loaders ===
//

const jsxLoader = {
	test: /\.jsx?$/,
	loader: 'babel-loader',
	include: path.resolve(ROOT_DIR, 'src')
};

//
// === Fonts & Images ===
//

const fontImageLoader = {
	test: /\.(woff(2)?|ttf|otf|eot|png|jpg|jpeg|svg|gif)$/,
	loader: 'file-loader',
};

//
// === HTML Loader ===
//

const handlebarLoader = {
	test: /\.hbs$/,
	loader: 'handlebars-loader',
	options: {
		inlineRequires: /\/assets\//ig,
	}
};

const htmlLoaderOptions = {
	template: path.resolve(ROOT_DIR, 'src', 'templates', 'index.hbs'),
	templateParameters: require('../src/databases/template.json'),
};

//
// === Config ===
//

module.exports = {
	entry: entrypoints,
	mode: 'production',
	output: {
		path: DIST_DIR,
		publicPath: '/',
		filename: '[name].js',
		chunkFilename: '[name].[chunkhash].js'
	},
	resolve: {
		extensions: ['.js', '.jsx'],
		alias: {
			react: 'preact-compat',
			'react-dom': 'preact-compat',
		},
	},
	module: {
		rules: [
			jsxLoader,
			sassLoader,
			fontImageLoader,
			handlebarLoader,
		]
	},
	optimization: {
		minimizer: [new TerserPlugin()]
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: '[name].css',
			chunkFilename: '[name].[chunkhash].css'
		}),
		new OptimizeCSSAssetsPlugin(),
		new HtmlWebpackPlugin(htmlLoaderOptions),
	],
};
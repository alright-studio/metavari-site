const path = require('path');
const reduce = require('lodash/reduce');
const fs = require('fs');
const autoprefixer = require('autoprefixer');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin')

//
// === Constants ===
//

const ROOT_DIR = path.resolve(__dirname, '..');
const ENTRYPOINT_DIR = path.resolve(ROOT_DIR, 'src', 'entrypoints');
const DIST_DIR = path.resolve(ROOT_DIR, 'dist');
const WEBPACK_PORT = 3000;
const BROWSERSYNC_PORT = 8080;

//
// === Get entrypoints from folder ===
//

const entrypointFileNames = fs.readdirSync(ENTRYPOINT_DIR).filter(file => {
	return file.match(/.*\.js$/);
});

const entrypoints = reduce(entrypointFileNames, (entrypoints, fileName) => {
	const entrypointName = fileName.replace('.js', '');

	return {
		...entrypoints,
		[entrypointName]: path.resolve(ENTRYPOINT_DIR, fileName),
	};
}, {});

//
// === Sass Loader Options ===
//

const postCSSOptions = {
	plugins: () => [
		autoprefixer({
			browsers: [
				'>1%',
				'last 4 versions',
				'not ie < 9'
			]
		})
	]
};

const sassLoaderOptions = {
	includePaths: [
		path.resolve(ROOT_DIR, 'node_modules'),
	]
};

const sassLoader = {
	test: /\.(scss|css)$/,
	use: [
		'style-loader',
		'css-loader',
		{
			loader: 'postcss-loader',
			options: postCSSOptions
		},
		{
			loader: 'sass-loader',
			options: sassLoaderOptions
		}
	],
	include: path.resolve(ROOT_DIR, 'src')
};

//
// === JS Loaders ===
//

const eslintLoader = {
	test: /\.jsx?$/,
	loader: 'eslint-loader',
	enforce: 'pre',
	exclude: path.resolve(ROOT_DIR, 'node_modules')
};

const jsxLoader = {
	test: /\.jsx?$/,
	loader: 'babel-loader',
	include: path.resolve(ROOT_DIR, 'src')
};

//
// === Fonts & Images ===
//

const fontImageLoader = {
	test: /\.(woff(2)?|ttf|otf|eot|png|jpg|jpeg|svg|gif)$/,
	loader: 'file-loader',
};

//
// === HTML Loader ===
//

const handlebarLoader = {
	test: /\.hbs$/,
	loader: 'handlebars-loader',
	options: {
		inlineRequires: /\/assets\//ig,
	}
};

const htmlLoaderOptions = {
	template: path.resolve(ROOT_DIR, 'src', 'templates', 'index.hbs'),
	templateParameters: require('../src/databases/template.json'),
};

//
// === Config ===
//

module.exports = {
	entry: entrypoints,
	mode: 'development',
	output: {
		path: DIST_DIR,
		publicPath: '/',
		filename: '[name].js',
		chunkFilename: '[name].[chunkhash].js'
	},
	resolve: {
		extensions: ['.js', '.jsx'],
		alias: {
			react: 'preact-compat',
			'react-dom': 'preact-compat',
		},
	},
	module: {
		rules: [
			eslintLoader,
			jsxLoader,
			sassLoader,
			fontImageLoader,
			handlebarLoader,
		]
	},
	plugins: [
		new HtmlWebpackPlugin(htmlLoaderOptions),
		new BrowserSyncPlugin({
			host: 'localhost',
			port: BROWSERSYNC_PORT,
			proxy: `http://localhost:${WEBPACK_PORT}`,
		}, {
			reload: true,
			injectCss: true,
		})
	],
	devServer: {
		hot: false,
		inline: false,
		port: WEBPACK_PORT,
		contentBase: path.resolve(ROOT_DIR, 'public'),
		headers: {
			'Access-Control-Allow-Origin': '*'
		}
	}
};